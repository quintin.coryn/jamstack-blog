import firebase from "firebase/compat";
import { initializeApp } from "firebase/app";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCjX0ZNx9vDRSathE7rPzakkVzaBNwfdf8",
  authDomain: "jamstackdemo-7b251.firebaseapp.com",
  projectId: "jamstackdemo-7b251",
  storageBucket: "jamstackdemo-7b251.appspot.com",
  messagingSenderId: "466964149455",
  appId: "1:466964149455:web:9fc8844c38d7d5bd7fb3ba"
}

let app;
if(firebase.apps.length === 0){
  app = firebase.initializeApp(firebaseConfig);
}else{
  app = firebase.app();
}
const analytics = getAnalytics(app);
const firestore = firebase.firestore()
const auth = firebase.auth()
export {firestore, auth}
